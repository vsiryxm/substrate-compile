# Substrate常用编译包

收集substrate节点编译包和智能合约wasm文件。

### 编译环境

1、操作系统 Ubuntu20.04 LTS

2、rust工具链
```
$ rustup show
Default host: x86_64-unknown-linux-gnu
rustup home:  /home/simon/.rustup

installed toolchains
--------------------

stable-x86_64-unknown-linux-gnu (default)
nightly-2021-03-01-x86_64-unknown-linux-gnu
nightly-x86_64-unknown-linux-gnu

installed targets for active toolchain
--------------------------------------

wasm32-unknown-unknown
x86_64-unknown-linux-gnu

active toolchain
----------------

stable-x86_64-unknown-linux-gnu (default)
rustc 1.51.0 (2fd73fabe 2021-03-23)
```